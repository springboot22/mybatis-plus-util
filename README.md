[TOC]



## 每个迭代完成的内容

1.0.0 完成实体与BaseMapper 的映射关系的缓存。

1.1.0 完成分页插件的学习和整合，将工具以Bean的方式注入到Spring容器中

1.2.0完成常用的插入删除更新分页操作的封装

## 工具使用指南

**注：目前是在Springboot项目中进行开发的**

1、保证每个实体类都已经有对应的继承了BaseMapper的接口，并导入util工具包下的所有内容

2、使用以下Java配置文件MybatisConfig.java

完成分页插件的注册和myBatisTutil工具的注册

```java
@Configuration
public class MybatisConfig {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        //不设置这个的话 分页查询的内容会出现 total = 0 的情况
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    @Bean
    public MyBatisTtUtil myBatisTutil(SqlSession sqlSession){
        return new MyBatisTtUtil(sqlSession);
    }
}
```

3、在需要用到常用的操作的地方直接注入service即可

```java
@Autowired
MyBatisTtService myBatisTtService;
```

4、常用查询和更新、删除、分页的示例可以查看

```java
ConditionController
```



## 重要内容详述

#### 1、MyBatisUtil

###### 此工具的前提

**必须要有引用了BaseMapper的接口**

```java
@Mapper
@Repository
public interface AppInfoMapper extends BaseMapper<AppInfo> {

}
```

###### 通过SqlSession 来加载 注册在MyBatisMapperRegistry里的 knownMappers

###### 通过内存来存储实体类与其对应的BaseMapper的关系

```java
/**
 * 实体类与它对应的Mapper的Class映射关系
 */
private final  Map<Class<?>, Class<?>> mapperCache = new HashMap<>(16);
```

###### 查到的knownMappers只是一个不可修改的视图

```java
@Override
public Collection<Class<?>> getMappers() {
    //此方法 生成一个原数据的不可修改的视图
    return Collections.unmodifiableCollection(knownMappers.keySet());
}
```

###### 通过 Type 等解析获取到的knownMappers

```java
public  void loadMapperCache(){
    Collection<Class<?>> mappers =  mapperRegistry.getMappers();
    mappers.forEach(mapper ->{
        //查找BaseMapper<T> 中的实体泛型类
        Type[] mapperType = mapper.getGenericInterfaces(); // generic 泛型
        // 强制转化“参数化类型”
        ParameterizedType parameterizedType = (ParameterizedType) mapperType[0];
        // 参数化类型中可能有多个泛型参数
        Type type = parameterizedType.getActualTypeArguments()[0];
        try {
            Class<?> entityClass =  Class.forName(type.getTypeName());
            mapperCache.put(entityClass,mapper);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    });
    System.out.println("加载 映射关系缓存"+mapperCache.size());
}
```

###### 条件构造器

```java
public class MyQueryWrapper<T> extends QueryWrapper<T> {
    private final Class<T> clazz;
    public MyQueryWrapper(Class<T> clazz){
        this.clazz = clazz;
    }
    public Class<T> getClazz(){
        return this.clazz;
    }
}
```

###### 无需再次传入实体类类型

```java
public <T,U extends BaseMapper<T>> List<T> getResult(QueryWrapper<T> queryWrapper,Class<T> entityClazz){
    U baseMapper = getMapperByEntity(entityClazz);
    return baseMapper.selectList(queryWrapper);
}

/**
 * 通过 实体类和查询构造条件进行查询
 * @param queryWrapper
 * @param <T>
 * @return
 */
public <T,U extends BaseMapper<T>> List<T> getResult(MyQueryWrapper<T> queryWrapper){
    U baseMapper = getMapperByEntity(queryWrapper.getClazz());
    return baseMapper.selectList(queryWrapper);
}
```

#### 2、MybatisConfig

dev 1.1.0中 将MybatisUtil改为MybatisTtUtil

将其原本的@Component去掉，在Config文件中以Bean的方式注入

```java
@Configuration
public class MybatisConfig {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        //不设置这个的话 分页查询的内容会出现 total = 0 的情况
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    @Bean
    public MyBatisTtUtil myBatisTutil(SqlSession sqlSession){
        return new MyBatisTtUtil(sqlSession);
    }
}
```

#### 3、MyBatisTtService

dev 1.2.0 

将公用查询Api放到Service中

提供了

- 单条查询
- 多条查询
- 分页查询
  - 提供了一个TtPageVo,使用者也可以自己来定义VO对象
- 插入
  - 通过实体对象来查询
- 更新
  - 通过UpdateWrapper
- 删除
  - 通过对象
  - 通过类型和id



```java
/**
 * @author will.tuo
 * @date 2021/12/16 15:25
 */
public interface MyBatisTtService {

    <T> List<T> getResult(MyQueryWrapper<T> queryWrapper);

    /**
     * 列表查询
     * @param queryWrapper 查询构造器
     * @param <T>          实体类型泛型
     * @return
     */
    <T> List<T> selectList(MyQueryWrapper<T> queryWrapper);

    /**
     * 单项查询
     * @param queryWrapper 查询构造器
     * @param <T>          实体类型泛型
     * @return
     */
    <T> T selectOne(MyQueryWrapper<T> queryWrapper);

    /**
     * 分页查询
     * @param queryWrapper 查询构造器
     * @param pageIndex    分页参数
     * @param pageSize     分页大小
     * @param <T>          实体类型泛型
     */
    <T> IPage<T> getPageResult(MyQueryWrapper<T> queryWrapper, int pageIndex, int pageSize);

    /**
     * 根据 whereEntity 条件，更新记录
     *
     * @param entity        实体对象 (set 条件值,可以为 null)
     * @param updateWrapper 实体对象封装操作类（可以为 null,里面的 entity 用于生成 where 语句）
     */
    <T> Object update(T entity, MyUpdateWrapper<T> updateWrapper);

    <T> Object update(MyUpdateWrapper<T> updateWrapper);

    <T> int insert(T entity);

    /**
     * 通过 id删除 ， 更可靠一些
     * @param clazz 实体类
     * @param id    id
     */
    <T> int delete(Class<T> clazz, int id);

    /**
     * 通过设置了 id 的实体对象；来删除，可靠性未知
     */
    <T> int delete(T entity);

}
```