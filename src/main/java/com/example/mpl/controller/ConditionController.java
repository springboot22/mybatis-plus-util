package com.example.mpl.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mpl.entity.AppInfo;
import com.example.mpl.util.service.MyBatisTtService;
import com.example.mpl.util.vo.TtPageVo;
import com.example.mpl.util.wrapper.MyQueryWrapper;
import com.example.mpl.util.wrapper.MyUpdateWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2021/12/14 10:51
 */
@RestController
public class ConditionController {

    @Autowired
    MyBatisTtService myBatisTtService;

    @RequestMapping("/geta")
    public Object geta(@RequestParam(value = "id") int id){
        MyQueryWrapper<AppInfo> queryWrapper = new MyQueryWrapper<>(AppInfo.class);
        queryWrapper.eq("app_id",id);
        return myBatisTtService.getResult(queryWrapper);
    }

    @RequestMapping("/page")
    public Object getPage(@RequestParam(value = "pageIndex") int pageIndex,@RequestParam(value = "pageSize") int pageSize,@RequestParam(value = "brandId") int brandId){
        MyQueryWrapper<AppInfo> queryWrapper = new MyQueryWrapper<>(AppInfo.class);
        queryWrapper.eq("brand_id",brandId);
        return new TtPageVo<>(myBatisTtService.getPageResult(queryWrapper,pageIndex,pageSize));
    }

    @RequestMapping("/up")
    public Object update(@RequestBody AppInfo appinfo){
        MyUpdateWrapper<AppInfo> updateWrapper = new MyUpdateWrapper<>(AppInfo.class);
        updateWrapper.eq("app_id",1);
        updateWrapper.set("status",1);
        return myBatisTtService.update(updateWrapper);
    }

    @RequestMapping("/de")
    public Object delete(@RequestParam(value = "id") int id){
        AppInfo appInfo = new AppInfo();
        appInfo.setAppId(id);
        return myBatisTtService.delete(appInfo);
    }

    @RequestMapping("/save")
    public Object save(){
        AppInfo appInfo = new AppInfo();
        appInfo.setAppName("拓朝山地车返回你倒是");
        myBatisTtService.insert(appInfo);
        return appInfo;
    }
}
