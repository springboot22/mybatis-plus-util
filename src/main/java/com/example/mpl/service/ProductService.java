package com.example.mpl.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.mpl.entity.Product;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/4 14:11
 */
@Service
public interface ProductService extends IService<Product> {
}
