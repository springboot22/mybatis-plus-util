package com.example.mpl.util.wrapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * @author will.tuo
 * @date 2021/12/15 17:11
 */
public class MyQueryWrapper<T> extends QueryWrapper<T> {
    private final Class<T> clazz;
    public MyQueryWrapper(Class<T> clazz){
        this.clazz = clazz;
    }
    public Class<T> getClazz(){
        return this.clazz;
    }
}
