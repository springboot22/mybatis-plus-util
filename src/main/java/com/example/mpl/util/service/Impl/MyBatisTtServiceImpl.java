package com.example.mpl.util.service.Impl;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.mpl.util.MyBatisTtUtil;
import com.example.mpl.util.service.MyBatisTtService;
import com.example.mpl.util.wrapper.MyQueryWrapper;
import com.example.mpl.util.wrapper.MyUpdateWrapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/12/16 9:38
 */
@Service
public class MyBatisTtServiceImpl implements MyBatisTtService {

    @Autowired
    MyBatisTtUtil myBatisTtUtil;

    @Override
    public <T> List<T> getResult(MyQueryWrapper<T> queryWrapper){
        return myBatisTtUtil.getResult(queryWrapper);
    }

    @Override
    public <T> List<T> selectList(MyQueryWrapper<T> queryWrapper){
        return myBatisTtUtil.getMapperByEntity(queryWrapper.getClazz()).selectList(queryWrapper);
    }

    @Override
    public <T> T selectOne(MyQueryWrapper<T> queryWrapper){
        return myBatisTtUtil.getMapperByEntity(queryWrapper.getClazz()).selectOne(queryWrapper);
    }

    @Override
    public <T> IPage<T> getPageResult(MyQueryWrapper<T> queryWrapper,int pageIndex,int pageSize){
        return myBatisTtUtil.getPageResult(queryWrapper,pageIndex,pageSize);
    }

    @Override
    public <T> Object update(T entity,MyUpdateWrapper<T> updateWrapper){
        return myBatisTtUtil.getMapperByEntity(updateWrapper.getClazz()).update(entity,updateWrapper);
    }

    @Override
    public <T> Object update(MyUpdateWrapper<T> myUpdateWrapper){
        return update(null,myUpdateWrapper);
    }

    @Override
    public <T> int insert(T entity){
        BaseMapper<T> baseMapper = (BaseMapper<T>) myBatisTtUtil.getMapperByEntity(entity.getClass());
        return baseMapper.insert(entity);
    }

    @Override
    public <T> int delete(Class<T> clazz,int id){
        return myBatisTtUtil.getMapperByEntity(clazz).deleteById(id);
    }

    @Override
    public <T> int delete(T entity){
        return myBatisTtUtil.getMapperByEntity(entity.getClass()).deleteById(myBatisTtUtil.getIdByEntity(entity));
    }

}
