package com.example.mpl.util.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.mpl.util.wrapper.MyQueryWrapper;
import com.example.mpl.util.wrapper.MyUpdateWrapper;
import java.util.List;

/**
 * @author will.tuo
 * @date 2021/12/16 15:25
 */
public interface MyBatisTtService {

    <T> List<T> getResult(MyQueryWrapper<T> queryWrapper);

    /**
     * 列表查询
     * @param queryWrapper 查询构造器
     * @param <T>          实体类型泛型
     * @return
     */
    <T> List<T> selectList(MyQueryWrapper<T> queryWrapper);

    /**
     * 单项查询
     * @param queryWrapper 查询构造器
     * @param <T>          实体类型泛型
     * @return
     */
    <T> T selectOne(MyQueryWrapper<T> queryWrapper);

    /**
     * 分页查询
     * @param queryWrapper 查询构造器
     * @param pageIndex    分页参数
     * @param pageSize     分页大小
     * @param <T>          实体类型泛型
     */
    <T> IPage<T> getPageResult(MyQueryWrapper<T> queryWrapper, int pageIndex, int pageSize);

    /**
     * 根据 whereEntity 条件，更新记录
     *
     * @param entity        实体对象 (set 条件值,可以为 null)
     * @param updateWrapper 实体对象封装操作类（可以为 null,里面的 entity 用于生成 where 语句）
     */
    <T> Object update(T entity, MyUpdateWrapper<T> updateWrapper);

    <T> Object update(MyUpdateWrapper<T> updateWrapper);

    <T> int insert(T entity);

    /**
     * 通过 id删除 ， 更可靠一些
     * @param clazz 实体类
     * @param id    id
     */
    <T> int delete(Class<T> clazz, int id);

    /**
     * 通过设置了 id 的实体对象；来删除，可靠性未知
     */
    <T> int delete(T entity);

}
