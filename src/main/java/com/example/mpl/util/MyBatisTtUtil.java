package com.example.mpl.util;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.MybatisMapperRegistry;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mpl.util.wrapper.MyQueryWrapper;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;

/**
 * @author will.tuo
 * @date 2021/12/14 15:52
 */
public class MyBatisTtUtil {

    /**
     * 实体类与它对应的Mapper的Class映射关系
     */
    private final Map<Class<?>, Class<?>> mapperCache = new HashMap<>(16);
    private final Map<Class<?>, Field> idCache = new HashMap<>(16);

    private final SqlSession sqlSession;

    private final Configuration configuration;

    private final MybatisMapperRegistry mapperRegistry;

    public MyBatisTtUtil(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
        configuration = sqlSession.getConfiguration();
        mapperRegistry = (MybatisMapperRegistry) configuration.getMapperRegistry();
        initCache();
    }

    public void initCache() {
        loadMapperCache();
        loadIdCache();
    }

    public void loadMapperCache() {
        Collection<Class<?>> mappers = mapperRegistry.getMappers();
        mappers.forEach(mapper -> {
            Type[] mapperType = mapper.getGenericInterfaces(); // generic 泛型
            // 强制转化“参数化类型”
            ParameterizedType parameterizedType = (ParameterizedType) mapperType[0];
            // 参数化类型中可能有多个泛型参数
            Type type = parameterizedType.getActualTypeArguments()[0];
            try {
                Class<?> entityClass = Class.forName(type.getTypeName());
                mapperCache.put(entityClass, mapper);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        System.out.println("加载 映射关系缓存" + mapperCache.size());
    }

    public void loadIdCache() {
        mapperCache.keySet().forEach(entityClass -> {
            Field[] fields1 = entityClass.getDeclaredFields();
            for (Field field : fields1) {
                field.setAccessible(true);
                Annotation[] annotations = field.getDeclaredAnnotations();
                for (Annotation annotation : annotations) {
                    if (annotation.annotationType().getTypeName().equals(TableId.class.getTypeName())) {
                        idCache.put(entityClass, field);
                        break;
                    }
                }
            }
        });
        System.out.println("加载 id关系缓存" + idCache.size());
    }

    /**
     * 根据 实体类 获取它对应的Mapper 接口
     *
     * @param entity
     * @return
     */
    public <U, T extends BaseMapper<U>> Class<T> getMapperClassByEntity(Class<U> entity) {
        Class<?> clazz = mapperCache.get(entity);
        if (clazz == null) {
            try {
                loadMapperCache();
                clazz = mapperCache.get(entity);
                if (clazz == null) {
                    throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("出问题了, 未加载成功");
            }
        }
        return (Class<T>) clazz;
    }

    /**
     * 通过 BaseMapper 的type 来获取实际的BaseMapper 的代理
     *
     * @param type
     * @param <T>
     * @return
     */
    public <T> T getMapper(Class<T> type) {
        return configuration.getMapper(type, sqlSession);
    }

    /**
     * 通过实体类 获取对应的BassMapper<T> 的代理
     *
     * @param <T>
     * @param entityClazz
     * @return
     */
    public <T> BaseMapper<T> getMapperByEntity(Class<T> entityClazz) {
        return getMapper(getMapperClassByEntity(entityClazz));
    }

    public <T> int getIdByEntity(T entity) {
        Field field = idCache.get(entity.getClass());
        try {
            if (field == null) {
                if (mapperCache.get(entity.getClass()) == null) {
                    loadMapperCache();
                }
                loadIdCache();
                field = idCache.get(entity.getClass());
            }
            return (int) field.get(entity);
        } catch (Exception e) {
            System.out.println("有问题没加载出来");
        }
        return 0;
    }

    /**
     * 通过 实体类和查询构造条件进行查询
     *
     * @param queryWrapper
     * @param entityClazz
     * @param <T>
     * @return
     */
    public <T> List<T> getResult(QueryWrapper<T> queryWrapper, Class<T> entityClazz) {
        return getMapperByEntity(entityClazz).selectList(queryWrapper);
    }

    /**
     * 通过 实体类和查询构造条件进行查询 无需再传入实体类
     *
     * @param queryWrapper
     * @param <T>
     * @return
     */
    public <T> List<T> getResult(MyQueryWrapper<T> queryWrapper) {
        return getMapperByEntity(queryWrapper.getClazz()).selectList(queryWrapper);
    }

    public <T> IPage<T> getPageResult(MyQueryWrapper<T> queryWrapper, int pageIndex, int pageSize) {
        IPage<T> ipage = new Page<>(pageIndex, pageSize);
        return getMapperByEntity(queryWrapper.getClazz()).selectPage(ipage, queryWrapper);
    }
}
