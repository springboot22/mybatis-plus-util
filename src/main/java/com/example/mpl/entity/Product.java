package com.example.mpl.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author will.tuo
 * @date 2021/12/14 11:20
 */
@TableName("tve_product")
@Data
public class Product {
    @TableId(value = "id")
    private int id = 0;

    protected int brandId;

    protected String description;

    @TableField(value = "playlist_id")
    protected int playListId;

    protected int packageType;

    protected String productModel;

    /**
     * 产品类型,1：零售、2：批发
     */
    protected int productType = 0;

    /**
     * STB激活类型,1：预付费、2：后付费
     */
    protected int payMode = 0;

    /**
     * 二维码扫描使用的产品
     */
    protected int linkProduct;

    /**
     * 二维码扫描使用的产品
     */
    protected int freeTime;

    /**
     * 首开发布状态，只要有app发布过，就为1，且不能更改
     */
    private int releaseStatus;

    /**
     * 名称
     */

    protected String name = null;
    /**
     * 备注
     */

    protected String remark = null;
    /**
     * 可用状态，1：可用，0：不可用，默认可用
     */

    protected int status = 1;

    protected long creator = 0;

    protected int createTime;

    protected int updateTime;

    protected long updateUser = 0;

}
