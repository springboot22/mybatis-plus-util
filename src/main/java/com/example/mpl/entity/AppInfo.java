package com.example.mpl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author will.tuo
 * @date 2021/12/15 10:51
 */
@TableName("tve_app_info")
@Data
public class AppInfo {

    @TableId(type = IdType.AUTO)
    private int appId;

    private String appAlias; //appId 别名

    private String appName; //appName

    private int vnoId;

    private int brandId;

    private int productType;

    //10: stb,11:mobile
    private int terminalType;

    //1:预付费 2:后付费
    private int payMode;

    private String mobileLongUrl;  //stb app对应的android apk long url

    private String mobileShortUrl;  //stb app对应的android apk short url

    protected long updateUser;

    protected int createTime;

    protected int updateTime;

    protected int status = 1;

    protected String remark;
}
